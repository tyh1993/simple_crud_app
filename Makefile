setup:
	docker-compose build && docker-compose up -d
migrate:
	docker exec -ti simplecrud_web_1 php artisan migrate;
