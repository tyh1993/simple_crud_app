<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('training_main');
});

Route::get('table', 'Training@getAll');

Route::post('create', 'Training@create');

Route::get('delete', 'Training@delete');

Route::get('update', 'Training@getUpdate');

Route::post('update', 'Training@update');
