<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\TrainingProgram;
use Illuminate\Support\Facades\Log;
use App\TrainingPrograms as TProgram;

class Training extends Controller
{
    //

    public function create(TrainingProgram $request)
    {
        try {
            $data = $request->toArray();
            $data['created_at'] = date("Y-m-d H:i:s");
            unset($data["_token"]);
            Log::info("got data from submit " . print_r($data ,1));
            DB::table('training_program')->insert(
                $data
            );
            Log::info("successfully insert data to table");
            return view('training_table');
        } catch(\Exception $ex) {
            return view('execption', ['message' => $ex->getMessage()]);
        }
    }

    public function getAll($limit = 100)
    {
        try {
            //$results = DB::select('select id, program_name, trainer_name, rate, venue, hotel, start_date, end_date
            //from training_program limit ?', [$limit]);

            return view('training_table', ['results' => TProgram::all()]);
        } catch(\Exception $ex) {
            return view('execption', ['message' => $ex->getMessage()]);
        }
    }

    public function delete(Request $request)
    {
        try {
            if ($request->id) {
                $result = DB::delete('delete from training_program where id = ?', [$request->id]);
                if ($result) {
                    Log::info("delete from training program with id " . $request->id);
                }
                return view('training_table', ['results' => TProgram::all()]);
            }
        } catch(\Exception $ex) {
            return view('execption', ['message' => $ex->getMessage()]);
        }
    }

    public function getUpdate(Request $request)
    {
        try {
            if ($request->id) {
                $result = TProgram::find($request->id);
                return view('update', ['result' => $result]);
            }
        } catch(\Exception $ex) {
            return view('execption', ['message' => $ex->getMessage()]);
        }

    }

    public function update(Request $request)
    {
        try {
            if ($request->id)
            {
                $training_program = TProgram::find($request->id);
                $training_program->program_name = $request->program_name;
                $training_program->trainer_name = $request->trainer_name;
                $training_program->rate = $request->rate;
                $training_program->venue = $request->venue;
                $training_program->hotel = $request->hotel;
                $training_program->start_date = $request->start_date;
                $training_program->end_date = $request->end_date;
                $training_program->updated_at = date("Y-m-d H:i:s");
                $training_program->save();
                Log::info("successfully update data from table");
            }
            return view('training_table', ['results' => TProgram::all()]);
        } catch(\Exception $ex) {
            return view('execption', ['message' => $ex->getMessage()]);
        }
    }
}
