FROM php:7-apache

RUN apt-get update \
  && apt-get install -y \
     mysql-client vim

RUN a2enmod rewrite

RUN docker-php-ext-install pdo_mysql

COPY . /var/www/html/simpleCRUD
WORKDIR /var/www/html/simpleCRUD

