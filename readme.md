## Require tools
- [Docker](https://www.docker.com)
- [docker-compose](https://docs.docker.com/compose/)
- [composer](https://getcomposer.org/)
- [Makefile](https://www.gnu.org/software/make/manual/make.html)

## setup projects
Run following commands

setup environment
```
make setup
```
install required packages
```
composer install
```

run migrations
```
make migrate
```

after finished the setup and migration, access the web app
```
http://localhost:8000/
```
