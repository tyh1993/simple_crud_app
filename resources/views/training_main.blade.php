<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('scripts')
        <title>Simple CRUD</title>
    </head>
    <body>
        @include('banner')
        <h2>Create new training program</h2>
        <div>
            <form action="/create" method="post">
            {{ csrf_field() }}
            <span>Program</span>
            <input type="text" name="program_name" placeholder="Program's Name"/>
            <span>Trainer</span>
            <input type="text" name="trainer_name" placeholder="Trainer's Name"/>
            <br/>
            <br/>
            <span>Rate</span>
            <input type="text" name="rate" placeholder="Rate"/>
            <span>Venue</span>
            <input type="text" name="venue" placeholder="Venue"/>
            <br/>
            <br/>
            <span>Hotel</span>
            <input type="text" name="hotel" placeholder="Hotel"/>
            <br/>
            <br/>
            <span>Dates</span>
            <input type="text" name="start_date" placeholder="example: 2017-05-01"/> To
            <input type="text" name="end_date" placeholder="example: 2017-05-03"/>
            <br/>
            <br/>
            <input type="submit" value="Submit">
        </form>
        </div>

    </body>
</html>
