<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('scripts')
        <title>Simple CRUD</title>
    </head>
    <body>
        @include('banner')
        <h2>Update training program</h2>
        <div>
            <form action="/update" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $result->id }}"/>
            <span>Program</span>
            <input type="text" name="program_name" placeholder="Program's Name" value="{{ $result->program_name }}"/>
            <span>Trainer</span>
            <input type="text" name="trainer_name" placeholder="Trainer's Name" value="{{ $result->trainer_name }}"/>
            <br/>
            <br/>
            <span>Rate</span>
            <input type="text" name="rate" placeholder="Rate" value="{{ $result->rate }}"/>
            <span>Venue</span>
            <input type="text" name="venue" placeholder="Venue" value="{{ $result->venue }}"/>
            <br/>
            <br/>
            <span>Hotel</span>
            <input type="text" name="hotel" placeholder="Hotel" value="{{ $result->hotel }}"/>
            <br/>
            <br/>
            <span>Dates</span>
            <input type="text" name="start_date" placeholder="example: 2017-05-01" value="{{ $result->start_date }}"/> To
            <input type="text" name="end_date" placeholder="example: 2017-05-03" value="{{ $result->end_date }}"/>
            <br/>
            <br/>
            <input type="submit" value="Submit">
        </form>
        </div>

    </body>
</html>
