<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('scripts')
        <title>Simple CRUD</title>
    </head>
    <body>
        @include('banner')
        <h2>View all training program</h2>
        <div>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Program's Name</th>
                        <th>Trainer's Name</th>
                        <th>Rate</th>
                        <th>Venue</th>
                        <th>Hotel</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($results as $result)
                        <tr id="{{ $result->id }}">
                            <td>{{ $result->program_name }}</td>
                            <td>{{ $result->trainer_name }}</td>
                            <td>{{ $result->rate }}</td>
                            <td>{{ $result->venue }}</td>
                            <td>{{ $result->hotel }}</td>
                            <td>{{ $result->start_date }}</td>
                            <td>{{ $result->end_date }}</td>
                            <td>
                                <a href="/update?id={{ $result->id }}" class="item-action">
                                    edit
                                </a>
                                <a href="/delete?id={{ $result->id }}" class="item-action item-action-danger">
                                    delete
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </body>
</html>
